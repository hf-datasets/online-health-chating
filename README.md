---
license: pddl
task_categories:
- question-answering
- table-question-answering
language:
- zh
tags:
- healthcare
- chat
- llm
- medical
size_categories:
- 100K<n<1M
---
---
license: pddl
---

# Online Health Chating

This is the repository for the Online Health Chating project.
which is the dataset of [chathealth](https://github.com/NeroHin/ChatHealth.git) project.

> Alarm: This dataset isfor academic research only and any commercial use and clinical use is prohibited.

## Dataset

We used crawler to collect the data from the following websites:

- [KingNet](http://www.kingnet.com.tw/)

|  Item  |  Size  |
| :----: | :----: |
| Row | 91,735 |


- [問 8 健康咨詢](https://tw.wen8health.com/)

|  Item  |  Size  |
| :----: | :----: |
| Row | 4,919 |


- [臺灣 E 院](https://sp1.hso.mohw.gov.tw/doctor/)

|  Item  |  Size  |
| :----: | :----: |
| Row | 153,251 |

- [家庭醫生](https://www.familydoctor.com.cn/)

|  Item  |  Size  |
| :----: | :----: |
| Row | 577,849 |

## LLM Dataset
Then we concatenate the data and split it into train, dev set with 7:3 ratio.

- train.json
- dev.json

| question | answer | 
| :----: | :----: |
| e.g. 有什麼方法可以治療腎結石？ | 有的，腎結石的治療方法有很多種，包括藥物治療、手術治療、醫療治療、中醫治療等。 |

```json
{
    "question": "有什麼方法可以治療腎結石？",
    "answer": "有的，腎結石的治療方法有很多種，包括藥物治療、手術治療、醫療治療、中醫治療等。"
}
```